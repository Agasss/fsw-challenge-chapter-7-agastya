const {room} = require("../models")

exports.register = (req, res) => {
    res.render("register")
}

exports.home = (req, res) => {
    res.render("home")
}

exports.login = (req, res) => {
    res.render("login")
}

exports.playerhome = (req, res) => {
    const {username, token} = req.query;
    res.render("userhome", {username, token})
}

exports.game = (req, res) => {
    const {username, token} = req.query
    room.findAll().then((rooms) => {
        res.render('game', {rooms, username, token});
    });
}