const {user} = require("../models")

exports.showAll = (req, res) => {
    user.findAll({include: ["room"]}).then((data) => {
        res.json({status: "Successfully Show All Data", data});
    }).catch((err) => {
        res.status(500).json({status: "Failed Show Data", msg: err});
    })
}

exports.showUser = (req, res) => {
    const currentUser = req.user;
    res.json(currentUser)
}