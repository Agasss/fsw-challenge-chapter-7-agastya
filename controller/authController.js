const {user} = require("../models")

const format = (user) => {
    const {id, username} = user;

    return {
        id,
        username,
        token: user.generateToken()
    }
}

exports.register = (req, res) => {
    user.register(req.body)
    .then((data) => {        
        res.redirect(`/login`)
        // res.json({ status:"Register Success", data: data});
    })
    .catch((err) => {
        res.status(500).json({status: "Register Failed", msg: err})
    })
}

exports.login = (req, res) => {
    const {username} = req.body
    user.authentication(req.body)

    .then((data) => {
        // res.json({ status:"Login Success", data: format(data)});
        res.redirect(`/home?username=${username}&token=${format(data).token}`)
    })
    .catch((err) => {
        res.status(400).json({status: "Login Failed", msg: err})
    })
}