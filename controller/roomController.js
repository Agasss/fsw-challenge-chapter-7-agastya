const {room} = require("../models")

exports.create = (req, res) => {
    const {name} = req.body
    room.create({
        name,
        winner: null,
        result: null
    })
    .then((data) => {
        res.json({status:"Room Created", data})
    })
    .catch((err) => {
        res.status(400).json({status:"Failed Create Room", message: err})
    })
}

exports.findAll = (req, res) => {
    room.findAll().then((data) => {
        res.json({status: "Successfully Show All Room", data});
    }).catch((err) => {
        res.status(500).json({status: "Failed Show Room", msg: err});
    })
}