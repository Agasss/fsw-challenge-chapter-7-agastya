const {room, game} = require("../models")

const findwinner = (first, second) => {
    if(first === second){
        return "draw";
    }
    else if(first == 'rock'){
        if(second == 'paper'){
            return "lose"
        }else{
            return "win"
        }
    }
    else if(first == 'scissor'){
        if(second == 'rock'){
            return "lose"
        }else{
            return "win"
        }
    }
    else if(first == 'paper'){
        if(second == 'scissor'){
            return "lose"
        }else{
            return "win"
        }
    }
}

exports.multiplayer = async (req, res) => {
    const {UserId, RoomId, choice} = req.body;

    const isNewGame = await game.findAll({where: {RoomId: RoomId}})
    const isUserExist = await game.findOne({where: {RoomId: RoomId, UserId:UserId}})

    if (isNewGame.length === 0) {
        game.create({UserId: UserId, RoomId:RoomId, choice})
        .then((respon) => {
            res.status(201).json({status: "Create Game Success", respon})
        })
        .catch((err) => {
            res.status(400).json({status: "Create Game Failed", err})
        })
    } else if (isNewGame.length === 1){
        if (isNewGame.length===1){
            if (!isUserExist){
                const gameResult = findwinner(isNewGame[0].choice, choice)

                game.create({UserId, RoomId, choice})
                .then(() => {
                    room.update(
                        {result: gameResult, winner: isNewGame[0].UserId},
                        {where: {id: RoomId}}
                    )
                    .then(() => {
                        res.json({result: `Player One ${gameResult}`})
                    })
                    .catch((err) => {
                        res.status(400).json({status: "Create Game Failed", message:err})
                    })
                })
            }
            else {
                res.status(400).json({status:"Create Game Failed", message:"this player is inserted!"})
            }
        }
    }
}