'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.user.hasMany(models.room, { 
        foreignKey: "winner",
        as: "room",
      });
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({username, email, password, sex, birthdate}) => {
      const encryptedPassword = this.#encrypt(password);

      const isUserExist = this.findOne({where: {username: username}})

      if (!isUserExist){
        return this.create({
          username,
          email,
          password: encryptedPassword,
          sex,
          birthdate,
        })
      } else {
        return Promise.reject("Username Exist");
      }
      // this.create({username, email, password: encryptedPassword})
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username
      }

      const secret = "This is my secret"
      const token = jwt.sign(payload, secret)

      return token;
    }

    static authentication = async({username, password}) => {
      try {
        const user = await this.findOne({where: {username}});
        if (!user) return Promise.reject("user not found");

        const isPassValid = user.checkPassword(password);

        if (!isPassValid) return Promise.reject("Wrong Password")

        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    }
  }
  user.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    sex: DataTypes.STRING,
    birthdate: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};