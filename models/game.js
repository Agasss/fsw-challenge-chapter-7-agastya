'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.game.belongsTo(models.room, {
        foreignKey: "RoomId",
        as: "user"
      })
    }
  }
  game.init({
    UserId: DataTypes.INTEGER,
    RoomId: DataTypes.INTEGER,
    choice: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'game',
  });
  return game;
};