'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.room.belongsTo(models.user, {
        foreignKey: "winner",
        as: "user"
      })

      models.room.hasMany(models.game, { 
        foreignKey: "RoomId",
        as: "game",
      });
    }
  }
  room.init({
    name: DataTypes.STRING,
    winner: DataTypes.INTEGER,
    result: DataTypes.STRING
  
  }, {
    sequelize,
    modelName: 'room',
  });
  return room;
};