const router = require("express").Router();
const game = require("../controller/gameController");
const restrict = require("../middleware/restrict")

router.post("/multiplayer",restrict, game.multiplayer)

module.exports = router;