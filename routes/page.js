const router = require("express").Router();
const page = require("../controller/pageController");

router.get("/", page.home)
router.get("/home", page.playerhome)
router.get("/register", page.register)
router.get("/login", page.login)
router.get("/game", page.game)

module.exports = router