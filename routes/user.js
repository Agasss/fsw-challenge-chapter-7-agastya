const router = require("express").Router();
const user = require("../controller/userController");
const restrict = require("../middleware/restrict")

router.get("/findall",restrict, user.showAll)

module.exports = router