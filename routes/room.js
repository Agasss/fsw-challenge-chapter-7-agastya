const router = require("express").Router();
const room = require("../controller/roomController");
const restrict = require("../middleware/restrict")

router.post("/create",restrict, room.create)
router.get("/findAll", restrict, room.findAll)

module.exports = router;