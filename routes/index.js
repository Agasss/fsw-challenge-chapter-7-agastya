var express = require('express');
var router = express.Router();

const authRouter = require("./auth")
const pageRouter = require("./page")
const userRouter = require("./user")
const roomRouter = require("./room")
const gameRouter = require("./game")

/* GET home page. */
router.use("/auth", authRouter)
router.use("/", pageRouter)
router.use("/user", userRouter)
router.use("/room", roomRouter)
router.use("/game", gameRouter)

module.exports = router;
